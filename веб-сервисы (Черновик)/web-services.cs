#region usings
using System;
using System.Net;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using EleWise.ELMA.Model.Common;
using EleWise.ELMA.Model.Entities;
using EleWise.ELMA.Model.Types.Settings;
using EleWise.ELMA.Model.Attributes;
using EleWise.ELMA.Model.Managers;
using EleWise.ELMA.ComponentModel;
using EleWise.ELMA.Services.Public;
using EleWise.ELMA.Web.Service;
using System.Collections.Generic;
using System.Runtime.Serialization;
using EleWise.ELMA.API;
using EleWise.ELMA.Common.Models;
using EleWise.ELMA.ConfigurationModel;
using EleWise.ELMA.Model.Services;

using EleWise.ELMA.Content;
using EleWise.ELMA.Projects;
using EleWise.ELMA.Messages;
using EleWise.ELMA.Security;
using EleWise.ELMA.Security.Models;
using EleWise.ELMA.Messages.Models;

using EleWise.ELMA.Security.Services;
using EleWise.ELMA.Workflow.Managers;
using EleWise.ELMA.Workflow.Services;
using EleWise.ELMA.Extensions;
using EleWise.ELMA.Services;
using EleWise.ELMA.Workflow.Models;
using EleWise.ELMA.Workflow.Processes;
using System.IO;

using EleWise.ELMA.Content;

using EleWise.ELMA.Logging;

using EleWise.ELMA.Model.Services;
using EleWise.ELMA.Web.Service.v1;
#endregion

namespace OffcutWebService
{
    public class OffcutWebServiceController
    {
        [ServiceContract(Namespace = APIRouteProvider.ApiServiceNamespaceRoot)]
        [Description("Приемник запросов из мобильного приложения")]
        [WsdlDocumentation("Принимает информацию о перемещении остатков")]
        public interface IOffcutWebService
        {
            [OperationContract]
            [FaultContract(typeof(PublicServiceException))]
            [Description("Принимает данные о пользователе для авторизации")]
            [WsdlDocumentation("данные в формате Json Username:\"Логин пользователя\" Password:\"Пароль пользователя\"")]
            [WebInvoke(Method = "POST",
                       UriTemplate = "/Login", BodyStyle = WebMessageBodyStyle.Wrapped)]
            UserData Login(string Username, string Password);

            [OperationContract]
            [FaultContract(typeof(PublicServiceException))]
            [Description("Принимает данные о пользователе для регистрации")]
            [WsdlDocumentation("данные в формате Json Username:\"Логин пользователя\" Password:\"Пароль пользователя\" Email:\"Email пользователя\"")]
            [WebInvoke(Method = "POST",
                       UriTemplate = "/SignUp", BodyStyle = WebMessageBodyStyle.Wrapped)]
            UserData SignUp(string Username, string Password, string Email);
        }

        [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, MaxItemsInObjectGraph = int.MaxValue)]
        [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
        [ServiceKnownType("GetEntityKnownTypes", typeof(ServiceKnownTypeHelper))]
        [Component]
        [Uid(GuidS)]
        public class OffcutWebService : IOffcutWebService, IPublicAPIWebService
        {
            public const string GuidS = "";

            public virtual UserData Login(string Username, string Password)
            {
                try
                {
                    var usersData = EntityManager<SomeUsers>.Instance.Find(x => x.Username == Username);

                    if (usersData == null || usersData.Count == 0)
                    {
                        throw new Exception("Не найден материал с таким логином: " + Username);
                    }
                    UserData user = null;
                    foreach (var each in usersData)
                    {
                        if (each.Password == Password)
                        {
                            user = new UserData();
                            user.Login = Username;
                            user.PublicName = each.Email;
                            break;
                        }
                    }
                    if (user == null)
                        throw new Exception("Неверный пароль");

                    return user;
                }
                catch (Exception e)
                {
                    ThrowError(e, "", Username);
					var error = new UserData();
					error.Login = "error";
                    return error;
                }
            }

            public virtual UserData SignUp(string Username, string Password, string Email)

            {
                try
                {
                    var usersData = EntityManager<SomeUsers>.Instance.Find(x => x.Username == Username);
                    if (usersData != null && usersData.Count != 0)
                    {
                        foreach (var each in usersData)
                        {
                            if (each.Password == Password)
                            {
								throw new Exception("Пользователь с такими данными уже существует");
                            }
                        }
                    }
                    UserData user = new UserData();

                    var newUser = EntityManager<SomeUsers>.Create();
					newUser.Username = Username;
					newUser.Password = Password;
					newUser.Email = Email;
                    newUser.Save();

					user.Login = Username;
					user.PublicName = Password;

                    return user;
                }
                catch (Exception e)
                {
                    ThrowError(e, "", Username);
					var error = new UserData();
					error.Login = "error";
                    return error;
                }
            }

			public virtual Data GetCatalog()
			{
				var goods = EntityManager<Goods>.Instance.FindAll();
				
				var result = new Data();

				foreach(var each in goods)
				{
					var dataItem = new DataItem();
					dataItem.name = each.name;
					dataItem.price = each.price;
					dataItem.img = each.img;
					dataItem.amount = 0;
					dataItem.uid = each.uid;
					goods.Add(dataItem);
				}

				return goods;
			}

			public virtual void CreateSale(Data goods)
			{
				bool flag = true;
				if(goods!=null && goods.Count>0)
					foreach(var good in goods)
					{
						if(goods.amount>0)
							flag = false;
					}
				if(flag)
				{
					throw new Exception("В сделке нет элементов");
				}

				var sale = EntityManager<Sale>.Create();
			
				foreach(var good in goods)
				{
					sale.Goods.Add(good);
				}
				sale.Save();
			}

            private void ThrowError(Exception e, string LogCell, string LogOffcut)
            {
                var user = PublicAPI.Portal.Security.User.LoadByLogin("extapp");
                PublicAPI.Services.Security.RunByUser(user, () =>
                {
                    //var bodyContent = new StreamReader(body).ReadToEnd();
                    var startableProcess = EntityManager<ProcessHeader>.Instance.Find(x => x.Id == 2502L).FirstOrDefault();
                    if (startableProcess != null)
                    {
                        var process = startableProcess.Published;
                        var instance = WorkflowInstanceManager.Instance.Create();
                        instance.Process = process;

                        (instance.Context.AsDynamic()).Error = new EleWise.ELMA.Common.Models.ExceptionData(0, e.Message, e);
                        (instance.Context.AsDynamic()).Cell = LogCell;
                        (instance.Context.AsDynamic()).Offcut = LogOffcut;
                        var workflowService = Locator.GetServiceNotNull<IWorkflowRuntimeService>();
                        workflowService.Run(instance);
                    }
                });
            }
        }

        public class UserData
        {
            public string Login;
            public string PublicName;
        }

    }

}
