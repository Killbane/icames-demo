import express from "express";
import todoRoutes from "./routes/router";
import connection from "./db/config";
import bodyParser, { json, urlencoded } from "body-parser";
import cors from "cors";

const app = express();
const port = 5001

app.use(json());

app.use(urlencoded({ extended: true }));

app.use("/test", todoRoutes);

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

app.use(
  (
    err: Error,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    res.status(500).json({ message: err.message });
  }
);

connection
  .sync()
  .then(() => {
    console.log("Database successfully connected");
  })
  .catch((err) => {
    console.log("Error", err);
  });
app.listen(port, () => {
  console.log("Server started on port "+port);
});