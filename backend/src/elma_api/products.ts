import { getData, postData } from "./common_scripts";
import elma_config from "./elma_config";

export type Product = {
    guid: string,
    email: string,
    name: string,
    surname: string,
    phone: string
}

let AuthToken = ""

function getAuthToken(): Promise<string> {
    if (AuthToken.length === 0) {
        return authorise()
    }
    return updateToken()
}

function authorise(): Promise<string> {
    const url = elma_config.domen+'/API/REST/Authorization/LoginWith?username=' + elma_config.login
    try {
        let json = postData(url, JSON.stringify("password"), elma_config.token);
        return json.then(result => {
            AuthToken = result.AuthToken
            return result.AuthToken
        }, error => {
            console.log(`Не удалось создать пользователя: ${error}`);
            return null
        });
    }
    catch (err) {
        console.log(err)
        throw "Request ERROR"
    }
}
function updateToken(): Promise<string> {
    const url = elma_config.domen+'/API/REST/Authorization/CheckToken?token=' + AuthToken
    try {
        let json = getData(url);
        return json.then(result => {
            AuthToken = result.AuthToken
            return result.AuthToken
        }, error => {
            console.log(`Не удалось создать пользователя: ${error}`);
            return null
        });
    }
    catch (err) {
        console.log(err)
        throw "Request ERROR"
    }
}

export async function getProductsElma(): Promise<Product[]> {
    const url = elma_config.domen+`/PublicAPI/REST/EleWise.ELMA.SDK.Web/GetInform/NewUser`
    let token = await getAuthToken()
    try {
        let json = postData(url, "", null, token);
        return json.then(result => {
            return result
        }, error => {
            console.log(`Не удалось создать пользователя: ${error}`);
            return null
        });
    }
    catch (err) {
        console.log(err)
        throw "Request ERROR"
    }
}