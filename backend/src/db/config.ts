import { Sequelize } from "sequelize-typescript";
import { Products } from "../models/products";
import { Users } from "../models/users";

const connection = new Sequelize({
  dialect: "mssql",
  host: "127.0.0.1",
  username: "sa",
  password: "testbd",
  database: "icames-demo",
  logging: false,
  models: [Products,Users],
});

export default connection;