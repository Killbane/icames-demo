import { RequestHandler } from "express";
import { Users } from "../models/users";
import { v1 as uid } from "uuid"
import { registerUserElma } from "../elma_api/users";

interface IUser {
  Email: string,
  Name: string,
  Surname: string,
  Password: string,
  Phone: string
}

export const registerUser: RequestHandler = async (req, res, next) => {
  //const allProducts: Products[] = await Products.findAll();
  var user = req.body as IUser
  let count = 1

  res = res.setHeader("Access-Control-Allow-Origin", "*")
    .setHeader("Access-Control-Allow-Methods", "POST")
    .setHeader("Access-Control-Allow-Headers", "Origin, Content-Type, X-Auth-Token")

  if (user.Password === undefined || user.Name === undefined || user.Email === undefined || user.Surname === undefined) {
    console.log("user data not complete")
    console.log(JSON.stringify(user))
    return res
      .status(400)
      .json("ERROR: Wrong method input data");
  }

  try {
    count = await Users.count({
      where: { Email: user.Email },
    });
  }
  catch (err) {
    console.log("failed to find user")
    return res
      .status(500)
      .json("ERROR: DB Writing Error: " + err);
  }

  if (count > 0) {
    console.log("user found")
    return res
      .status(200)
      .json("ERROR: User with current Email already exists");
  }

  let someUid = uid()
  console.log(someUid)

  Users.create({
    Email: user.Email,
    Password: user.Password,
    FirstName: user.Name,
    Surname: user.Surname,
    Uid: someUid,
    CellPhone: user.Phone
  }).then(() => {
    console.log("Successfully registered")

    registerUserElma({
      guid: someUid,
      email: user.Email,
      name: user.Name,
      surname: user.Surname,
      phone: user.Phone
    }).catch(err=>{
      console.log("Elma SignUp ERROR: "+err)
    })

    return res
      .status(200)
      .json(someUid);
  }).catch(err => {
    console.log("DB Writing Error: " + err)
    return res
      .status(500)
      .json("ERROR: " + err);
  })
};

export const loginUser: RequestHandler = async (req, res, next) => {
  //const allProducts: Products[] = await Products.findAll();
  var user = req.body as IUser
  let foundUser: Users | null

  res = res.setHeader("Access-Control-Allow-Origin", "*")
    .setHeader("Access-Control-Allow-Methods", "POST")
    .setHeader("Access-Control-Allow-Headers", "Origin, Content-Type, X-Auth-Token")

  if (user.Password === undefined || user.Email === undefined) {
    console.log("user data not complete")
    return res
      .status(400)
      .json("ERROR: Wrong method input data");
  }

  try {
    foundUser = await Users.findOne({ where: { Email: user.Email }, })
  }
  catch {
    console.log("DB Error")
    return res
      .status(200)
      .json(null);
  }

  if (foundUser !== null) {
    if (foundUser.Password === user.Password) {
      console.log("Successfully logged in " + foundUser.Uid + " " + Date.now())
      return res
        .status(200)
        .json(foundUser.Uid);
    }
    else {
      console.log("Wrong user Password")
      return res
        .status(200)
        .json("ERROR: Wrong user Password");
    }
  }

  console.log("Wrong user data")
  return res
    .status(200)
    .json("ERROR: Wrong Email");
};