import { RequestHandler } from "express";

import { Products } from "../models/products";

export const getCatalog: RequestHandler = async (req, res, next) => {
  const allProducts: Products[] = await Products.findAll();
  
  return res
    .setHeader("Access-Control-Allow-Origin","*")
    .json(allProducts )
    .status(200);
};