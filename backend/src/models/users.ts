import { Table, Model, Column, DataType } from "sequelize-typescript";

@Table({
  timestamps: false,
  tableName: "users",
})
export class Users extends Model {
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  Email!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  Password!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  CellPhone!: string;

  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  FirstName!: string;

  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  Surname!: string;
  
  @Column({
    type: DataType.UUID,
    allowNull: true,
  })
  Uid!:string;

}