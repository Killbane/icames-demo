import { Router } from "express";

import {
  getCatalog
} from "../controller/products";
import { registerUser, loginUser } from "../controller/users";

const router = Router();

router.get("/catalog", getCatalog);
router.post("/register", registerUser);
router.post("/login", loginUser);

router.options("/register", function(req, res, next){
  console.log("asked options")
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.sendStatus(200);
});

router.options("/login", function(req, res, next){
  console.log("asked options")
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.sendStatus(200);
});

export default router;