import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Main from './pages/Main';
import Cart from './pages/Cart';
//import Home from '../pages/Home';
import Signup from './pages/Signup';
import { Product } from './scripts/product';
import ErrorBoundary from './scripts/error_bundary';

let products:Product[] = []
let user=""

class App extends React.Component{
  
  updateProducts(new_products:Product[]){
    products = new_products;
  }
  
  getProducts():Product[]{
    return products;
  }

  updateUser(new_user:string){
    user = new_user
  }

  getUser():string{
    return user
  }

  render(): React.ReactNode {
    return (
      <Routes> 
        <Route path='/' element={<ErrorBoundary><Main getter={this.getProducts} setter={this.updateProducts} getUser={this.getUser}/></ErrorBoundary>}></Route>
        <Route path='/signup' element={<ErrorBoundary><Signup  getUser={this.getUser}  setUser={this.updateUser}/></ErrorBoundary>}></Route>
        <Route path='/cart' element={<ErrorBoundary><Cart getter={this.getProducts} setter={this.updateProducts} getUser={this.getUser}/></ErrorBoundary>}></Route>
      </Routes>
    );
  }
}

export type getProduct = () => Product[];
export type setProduct = (new_products:Product[]) => void;
export type getUser = () => string;
export type setUser = (new_products:string) => void;

export default App;
