export async function getData(url: string)  {
    try{
        const response = await fetch(url, {
            //mode: "no-cors",
            method: 'GET',
            keepalive: true,
            referrerPolicy: "no-referrer-when-downgrade",
            headers: {
            Accept: 'application/json;charset=UTF-8'
        }});
        return response.json();
    } catch (err){
        throw `Ошибка GET запроса по адресу ${url.substring(0, 50)}...`;
    }
}

export async function postData(url: string, body: string)  {
    try{
        let headers = new Headers({
            "Content-Type": "application/json;charset=UTF-8",
            "Accept":"application/json;charset=UTF-8"
        })

        let req:RequestInit = {
            //mode: "no-cors",
            method: "POST",
            keepalive: true,
            body: body,
            referrerPolicy: "no-referrer",
            headers:headers 
        }

        const response = await fetch(url, req).catch(err=>{
            throw `Ошибка POST запроса по адресу ${err}...`;
        });
        return response.json();
    } catch (err){
        throw `Ошибка POST запроса по адресу ${url.substring(0, 50)}...\n${err}`;
    }
}