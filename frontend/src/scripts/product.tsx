import { getData} from "./common_scripts";

export type Product = {
    name:string,
    price:number,
    img:string,
    amount:number,
    uid:string
}

export function getProducts(): Promise<Product[]> {
    const url = `http://127.0.0.1:5001/test/catalog`
    let json = getData(url);
    return json.then(result => {
        return result.map((e : Product) => {
            return {
                name: e.name,
                img: e.img,
                price: e.price,
                amount: 0,
                uid:e.uid
            };
        });
    }, error => {
        throw `Не удалось получить продукты для каталога: ${error}`;
    });
}