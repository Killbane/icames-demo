import { postData} from "./common_scripts";

export function loginUser(body: object): Promise<string> {
    const url = `http://127.0.0.1:5001/test/login`
    let json = postData(url,JSON.stringify(body));
    return json.then(result => {
        return result;
    }, error => {
        console.log(`Не удалось авторизоваться: ${error}`);
        return null;
    });
}

export function registerUser(body:object): Promise<string>{
    const url = `http://127.0.0.1:5001/test/register`
    try{
        let json = postData(url,JSON.stringify(body));
        return json.then(result => {
            return result
        }, error => {
            console.log(`Не удалось создать пользователя: ${error}`);
            return null
        });
    }
    catch(err){
        console.log(err)
        throw "Request ERROR"
    }
}