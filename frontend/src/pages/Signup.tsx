import '../style_sheets/style_signup.css'
import { Link, Navigate } from "react-router-dom";
import React from 'react';
import { loginUser, registerUser } from '../scripts/user';
import { threadId } from 'worker_threads';
import { getUser, setUser } from '../App';

class UserState{
    user:string|null=null
}

type IProps = {
    getUser:getUser,
    setUser:setUser
}

export default class Signup extends React.Component<IProps,UserState> {
    
    constructor(props: IProps) {
        super(props);

        this.state = {
            user: ""
        }

        this.passwordOnChange = this.passwordOnChange.bind(this);
        this.emailOnChange = this.emailOnChange.bind(this);
        this.nameOnChange = this.nameOnChange.bind(this);
        this.surnameOnChange = this.surnameOnChange.bind(this);
        this.phoneOnChange = this.phoneOnChange.bind(this);
        this.login = this.login.bind(this)
        this.register = this.register.bind(this)
        this.redirectToMain = this.redirectToMain.bind(this)
    }

    Email: string = ""
    Password: string = ""
    Name: string = ""
    Surname: string = ""
    Phone: string = ""

    login(e: { preventDefault: () => void; } | undefined) {
        if (this.isNullOrEmpty(this.Email) ||
            this.isNullOrEmpty(this.Password))
            return;
        let user = {
            Email:this.Email,
            Password:this.Password
        }

        e!.preventDefault()
        
        loginUser(user).then(result=>{
            this.props.setUser(result)
            this.setState({
                user:result
            })
        }).catch(error=>{
            console.log(error)
        })
    }

    register(e: { preventDefault: () => void; } | undefined) {
        if (this.isNullOrEmpty(this.Email) ||
            this.isNullOrEmpty(this.Password)||
            this.isNullOrEmpty(this.Name)||
            this.isNullOrEmpty(this.Surname)||
            this.isNullOrEmpty(this.Phone))
            return;
        
        let user = {
            Name:this.Name,
            Surname:this.Surname,
            Email:this.Email,
            Password:this.Password,
            Phone:this.Phone
        }

        e!.preventDefault()
        registerUser(user).then(result=>{
            this.props.setUser(result)
            this.setState({
                user:result
            })
        }).catch(error=>{
            console.log(error)
        })
    }

    isNullOrEmpty(str: string): boolean {
        return (str === null || str.length === 0)
    }

    passwordOnChange(val: React.FormEvent<HTMLInputElement>)
    {
        this.Password = val.currentTarget.value
    }

    emailOnChange(val: React.FormEvent<HTMLInputElement>)
    {
        this.Email = val.currentTarget.value
    }

    nameOnChange(val: React.FormEvent<HTMLInputElement>)
    {
        this.Name = val.currentTarget.value
    }

    surnameOnChange(val: React.FormEvent<HTMLInputElement>)
    {
        this.Surname = val.currentTarget.value
    }

    phoneOnChange(val: React.FormEvent<HTMLInputElement>)
    {
        this.Phone = val.currentTarget.value
    }

    redirectToMain()
    {
        if(this.state.user!==undefined && this.state.user!==null && this.state.user.length >0)
            return <Navigate to="/"/>
    }

    render() {
        return (
            <>
                <div className="ReturnToMain">
                    <Link className='button' to="/" style={{ textDecoration: 'none' }} type='button'>
                        На главную
                    </Link>
                </div>
                {this.redirectToMain()}
                <div className="main">
                    <input className='input' type="checkbox" id="chk" aria-hidden="true" />

                    <div className="signup">
                        <form>
                            <label className='label' htmlFor="chk" aria-hidden="true">Вход</label>
                            <input className='input' type="email" placeholder="Email" onChange={this.emailOnChange} required={true} />
                            <input className='input' type="password" placeholder="Пароль" onChange={this.passwordOnChange} required={true} />
                            <button className='button' onClick={(e)=>this.login(e)}>Войти</button>
                        </form>
                    </div>

                    <div className="login">
                        <form>
                            <label className='label' htmlFor="chk" aria-hidden="true">Регистрация</label>
                            <input className='input' type="text" placeholder="Имя" onChange={this.nameOnChange} required={true} />
                            <input className='input' type="text" placeholder="Фамилия" onChange={this.surnameOnChange} required={true} />
                            <input className='input' type="text" placeholder="Телефон" onChange={this.phoneOnChange} required={true} />
                            <input className='input' type="email" placeholder="Email" onChange={this.emailOnChange} required={true} />
                            <input className='input' type="password" placeholder="Пароль" onChange={this.passwordOnChange} required={true} />
                            <button className='button' onClick={(e)=>this.register(e)}>Зарегистрироваться</button>
                        </form>
                    </div>
                </div>
            </>
        );
    }
}
