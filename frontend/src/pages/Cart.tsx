import '../style_sheets/style_cart.css'
import { Link } from "react-router-dom";
import React from 'react';
import { Product } from '../scripts/product';
import {getProduct as get, setProduct as set, getUser} from '../App'

interface IProps {
    getter:get
    setter:set
    getUser:getUser
}

class states {
    user: string | null = null
    chousenProducts: Product[] = []
    fullPrice: number = 0;
}

export default class Cart extends React.Component<IProps,states> {

    constructor(props: IProps) {
        super(props);
        
        this.state = {
            user:props.getUser(),
            chousenProducts: props.getter(),
            fullPrice: 0
        };

        this.encountFullPrice = this.encountFullPrice.bind(this);
    }

    encountFullPrice() {
        let fullPrice = 0;
        this.state.chousenProducts.forEach(element => {
            fullPrice += element.amount * element.price;
        });
        if (fullPrice === this.state.fullPrice)
            return;
        this.setState({
            user:this.state.user,
            chousenProducts: this.state.chousenProducts,
            fullPrice: fullPrice
        });
    }

    componentDidMount(){
        if((this.state as states).fullPrice===0 && (this.state as states).chousenProducts && (this.state as states).chousenProducts.length>0)
            this.encountFullPrice()
    }

    onAmountChange(newValue:string, item:Product) {
        item.amount = Number.parseInt(newValue)
        this.encountFullPrice()
    }

    render() {
        return (<div className="grid-container_cart">
                <link rel="stylesheet" href="../style_sheets/style_cart.css"/>
                <div className="post-1_cart">
                    <div className='button_area_cart'>
                    <Link to="/" className="button_main_cart button_cart" style={{textDecoration: 'none'}} type='button' state={{ chousenProducts: (this.state as states).chousenProducts, fullPrice: (this.state as states).fullPrice }}>
                        На главную
                    </Link></div>
                </div>
                <div className="post-2_cart">
                    <div className="scroll_elem_cart">
                        <ul>{
                                (this.state as states).chousenProducts.map((item) => {
                                    return <li className="item_line_cart">
                                        <div className="item_img_cart">
                                            <img src={item.img} alt="" />
                                        </div>
                                        <div className="item_number_cart">
                                            <div className="count_of_product_cart">
                                                <div className="text_number_cart">Кол-во</div>
                                                <div className="text_count_cart">
                                                    <input type="number" step="1" min="0" value={item.amount} onChange={event =>this.onAmountChange(event.target.value, item)} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="item_discription_cart">
                                            <div className="item_name_cart">{item.name}</div>
                                            <div className="item_price_cart">Цена: {item.price} руб.</div>
                                        </div>
                                    </li>
                                        ;
                                })}
                        </ul>
                    </div>
                </div>
                <div className="post-3_cart">
                    <div className="container_place_an_order_cart">
                        <div className="place_an_order_cart">Оформить заказ</div>
                        <div className="penal_of_procces_an_order_cart">
                            <div className="order_sum_cart">Cумма заказа</div>
                            <div className="sum_cart">
                                <div className="price_cart"> { (this.state as states).fullPrice}</div>
                            </div>
                            <button className="button_order_cart button_cart">Заказать</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}