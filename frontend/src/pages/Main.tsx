import '../style_sheets/style_catalog.css'
import { Link, Navigate } from "react-router-dom";
import React from 'react';
import { getProducts, Product } from '../scripts/product';
import {getProduct as get, setProduct as set, getUser} from '../App'

interface IProps {
    getter: get
    setter: set
    getUser:getUser
}

class states {
    user: string | null = null
    data: Product[] = []
    chousenProducts: Product[] = []
    isLoaded: boolean = false;
    hasError: boolean = false;
    loadError: string = "";
}

export default class Main extends React.Component<IProps, states> {
    constructor(props: IProps) {
        super(props);

        if (this.state === undefined)
            this.state = {
                user: props.getUser(),
                data: [],
                chousenProducts: props.getter(),
                isLoaded: false,
                hasError: false,
                loadError: ""
            };

        this.loadItems = this.loadItems.bind(this);
        this.handleItemClick = this.handleItemClick.bind(this);
        this.renderItem = this.renderItem.bind(this);
        this.renderHeader = this.renderHeader.bind(this);
        this.redirectToSignup = this.redirectToSignup.bind(this);
    }

    renderItem(item: Product) {
        if (this.state.chousenProducts.some(x => x.uid === item.uid))
            return (<button className="item_have_added_to_cart_main button_main" onClick={() => this.handleItemClick(item)}>Добавлено</button>)
        return (<button className="item_add_to_cart_main button_main" onClick={() => this.handleItemClick(item)}>Добавить в корзину</button>)
    }

    handleItemClick(item: Product) {
        var index = this.state.chousenProducts.findIndex((x) => x.uid === item.uid);
        if (index === -1)
            this.state.chousenProducts.push(item);
        else
            this.state.chousenProducts.splice(index, 1);
        this.props.setter(this.state.chousenProducts)
        this.setState({ chousenProducts: this.state.chousenProducts, data: this.state.data });
    }

    async loadItems() {
        let result: Product[] = []
        let product: Product = { img: "resource/closet.jpg", amount: 0, name: "Generated", price: 1232, uid: "1023123" }
        result.push(product)
        /*this.setState({
            data: result,
            chousenProducts: this.state.chousenProducts,
            isLoaded: true
        })*/

        getProducts().then(result => {
            this.setState({
                user: this.state.user,
                data: result,
                chousenProducts: this.state.chousenProducts,
                isLoaded: true,
            })
        },
            error => {
                this.setState({
                    user: this.state.user,
                    chousenProducts: this.state.chousenProducts,
                    hasError: true,
                    loadError: error,
                    isLoaded: true,
                })
            })
    }

    componentDidMount() {
        if (this.state === undefined || !this.state.isLoaded)
            this.loadItems()
    }

    redirectToSignup(){
        if (this.state === undefined || this.state === null || this.state.user === undefined || this.state.user === null || this.state.user.length === 0)
            return <Navigate to="/signup" />
    }

    renderHeader(): JSX.Element {
        const mainIcon: React.CSSProperties = {
            width: '36px',
            height: '36px'
        }

        return (
            <>
                {this.redirectToSignup()}
                <div className="post-1_main">
                    <div className="button_area_main">
                        <Link className="button_main button_main_main" style={{ textDecoration: 'none' }} to="/signup" type='button'>
                            {"Войти"}
                        </Link>
                        <Link className="button_main button_main_main" style={{ textDecoration: 'none' }} to="/cart" state={{ chousenProducts: this.state.chousenProducts }} type='button'>
                            <svg style={mainIcon} viewBox="0 0 24 24">
                                <path fill="white" d="M19 20C19 21.11 18.11 22 17 22C15.89 22 15 21.1 15 20C15 18.89 15.89 18 17 18C18.11 18 19 18.9 19 20M7 18C5.89 18 5 18.89 5 20C5 21.1 5.89 22 7 22C8.11 22 9 21.11 9 20S8.11 18 7 18M7.2 14.63L7.17 14.75C7.17 14.89 7.28 15 7.42 15H19V17H7C5.89 17 5 16.1 5 15C5 14.65 5.09 14.32 5.24 14.04L6.6 11.59L3 4H1V2H4.27L5.21 4H20C20.55 4 21 4.45 21 5C21 5.17 20.95 5.34 20.88 5.5L17.3 11.97C16.96 12.58 16.3 13 15.55 13H8.1L7.2 14.63M8.5 11H10V9H7.56L8.5 11M11 9V11H14V9H11M14 8V6H11V8H14M17.11 9H15V11H16L17.11 9M18.78 6H15V8H17.67L18.78 6M6.14 6L7.08 8H10V6H6.14Z" />
                            </svg>
                        </Link>
                    </div>
                </div>
            </>)
    }

    render() {

        if (this.state === undefined || !this.state.isLoaded) {
            return (
                <>
                    <div>
                        {this.renderHeader()}

                        <p className='p_notify'>{"Загрузка..."}</p>
                    </div>
                </>);
        }
        if (this.state.hasError) {
            return (<>
                <div>
                    {this.renderHeader()}
                    <p className='p_notify'>{this.state.loadError.toString()}</p>
                </div>
            </>);
        }

        return (<>
            <div className="grid-container_main">
                <link rel="stylesheet" href="../style_sheets/style_catalog.css" />
                {this.renderHeader()}
                <div className="post-2_main">
                    <div className="scroll_elem_main">
                        <ul>
                            {
                                this.state.data.map((item) => {
                                    return <li className="item_line_main" key={"main_item_cell" + item.name}>
                                        <div className="item_img_main">
                                            <img src={item.img} alt="" />
                                        </div>
                                        <div className="item_button_main">
                                            {this.renderItem(item)}
                                        </div>
                                        <div className="item_discription_main">
                                            <div className="item_name_main">{item.name}</div>
                                            <div className="item_price_main">Цена: {item.price} руб.</div>
                                        </div>
                                    </li>
                                        ;
                                })}
                        </ul>
                    </div>
                </div>
            </div>
        </>
        );
    }
}